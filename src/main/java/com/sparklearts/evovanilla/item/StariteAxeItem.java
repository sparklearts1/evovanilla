
package com.sparklearts.evovanilla.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.world.World;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.item.AxeItem;
import net.minecraft.entity.Entity;

import java.util.Map;
import java.util.HashMap;

import com.sparklearts.evovanilla.procedures.StariteToolsInHandTickProcedure;
import com.sparklearts.evovanilla.EvoVanillaModElements;

@EvoVanillaModElements.ModElement.Tag
public class StariteAxeItem extends EvoVanillaModElements.ModElement {
	@ObjectHolder("evo_vanilla:starite_axe")
	public static final Item block = null;
	public StariteAxeItem(EvoVanillaModElements instance) {
		super(instance, 28);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new AxeItem(new IItemTier() {
			public int getMaxUses() {
				return 4062;
			}

			public float getEfficiency() {
				return 12f;
			}

			public float getAttackDamage() {
				return 8f;
			}

			public int getHarvestLevel() {
				return 5;
			}

			public int getEnchantability() {
				return 25;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(Items.NETHER_STAR));
			}
		}, 1, -2.9f, new Item.Properties().group(ItemGroup.TOOLS).isImmuneToFire()) {
			@Override
			public void inventoryTick(ItemStack itemstack, World world, Entity entity, int slot, boolean selected) {
				super.inventoryTick(itemstack, world, entity, slot, selected);
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();
				if (selected) {
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					StariteToolsInHandTickProcedure.executeProcedure($_dependencies);
				}
			}
		}.setRegistryName("starite_axe"));
	}
}
